package top.healthylife.dochelper;

import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;
import top.healthylife.dochelper.DocHelper;
import top.healthylife.dochelper.config.DocBaseConfig;

import java.io.File;

/**
 * 功能描述: DocTest
 *
 * @author maoxiaomeng
 * @date 20230609
 */
@Slf4j
public class DocTest {

    static String path = "C:\\Users\\maoxiaomeng\\Desktop\\担保\\final\\test\\DocHelpers数据中台_配置表.xlsx";

    @Test
    public void test01() {
        DocHelper.start(new File(path));
    }

    @Test
    public void test02() {
        DocBaseConfig.initLocalConfig(DocHelper.configFile);
    }

    @Test
    public void test03() {
        DocHelper.start(DocHelper.configFile);
    }

}
